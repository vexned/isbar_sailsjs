/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'main'
  } ,
  '/country/get':'CountryController.get',
  '/country/getSql':'CountryController.getSql',

  '/staff/get':'StaffController.get',
  '/position/get':'PosController.get',
  '/staff/getSql':'StaffController.getSql',
  '/position/getSql':'PosController.getSql',
  '/staff':'StaffController.show',
  'POST /staff/update/:id':'StaffController.update',
  'POST /staff/new':'StaffController.new',
  '/staff/add':'StaffController.add',
  '/staff/delete/:id':'StaffController.delete',

  '/position':'PosController.show',
  '/position/edit/:id':'PosController.edit',
  'POST /position/update/:id':'PosController.update',
  '/position/add':'PosController.add',
  'POST /position/new':'PosController.new',
  '/position/delete/:id':'PosController.delete',

  '/requisites/:id':'RequisitesController.show',
  '/requisites/edit/:id':'RequisitesController.edit',
  '/requisites/update/:id':'RequisitesController.update',

  '/provider':'ProviderController.show',
  '/provider/assortment/:id':'AssortmentController.show',
  '/provider/assortment/:id/add':'AssortmentController.add',
  '/provider/assortment/:id/new':'AssortmentController.new',
  '/provider/assortment/:id/delete/:gid':'AssortmentController.delete',
  '/provider/edit/:id':'ProviderController.edit',
  'POST /provider/update/:id':'ProviderController.update',
  '/provider/delete/:id':'ProviderController.delete',
  '/provider/add':'ProviderController.add',
  'POST /provider/new':'ProviderController.new',

  '/goods':'GoodsController.show',
  '/goods/edit/:id':'GoodsController.edit',
  '/goods/update/:id':'GoodsController.update',
  '/goods/add':'GoodsController.add',
  'POST /goods/new':'GoodsController.new',

  '/unit':'UnitController.show',
  '/unit/edit/:id':'UnitController.edit',
  '/unit/update/:id':'UnitController.update',
  '/unit/delete/:id':'UnitController.delete',


  '/gis':'GoodsinstockController.show',
  '/gis/add':'GoodsinstockController.add',
  '/gis/assortment/:id':'AssortmentController.get',
  '/gis/count/:id':'GoodsinstockController.count',
  '/gis/edit/:id':'GoodsinstockController.edit',
  '/gis/edit/:gid/getone/:pid':'GoodsinstockController.getone',
  '/gis/delete/:id':'GoodsinstockController.delete',
  'POST /gis/new':'GoodsinstockController.new',

  '/drink':'DrinkController.show',
  '/drink/recipe/:id':'RecipeController.show',
  '/drink/add':'DrinkController.add',
  'POST /drink/new':'DrinkController.new',
  '/drink/delete/:id':'DrinkController.delete',

  '/snack':'SnackController.show',
  '/snack/recipe/:id':'RecipeController.showsnack',
  'POST /snack/new':'SnackController.new',
  '/pricelist':'PricelistController.show',
  '/gis/doc':'GoodsinstockController.showdoc',

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
