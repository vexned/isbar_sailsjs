/**
 * Country.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'someMysqlServer',
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    country_id :{
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      required: true
    },

    country: {
      type: 'string',
      required: true,
      size: 50
    },
    
    last_update: {
      type: 'datetime',
      required: true
    }

  }
};

