/**
 * Assortment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'someMysqlServer',
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  schema: true,

  attributes: {
    price: {
      type:'float',
      required: true,
    },
    cond: {
      type:'string',
      required: true,
      size: 255,
    },
    payment: {
      type:'string',
      required: true,
      size: 255,
    },
    id_provider: {
      type:'integer',
      model:'provider'
    },
    id_goods: {
      type:'integer',
      model:'goods'
    },
  }
};

