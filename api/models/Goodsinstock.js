/**
 * Goodsinstock.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  schema: true,
  attributes: {
    AWPrice: {
      type: 'float',
      required: true,
    },
    quantity: {
      type: 'integer',
      required: true,
    },
    id: {
      type:'integer',
      model:'goods',
    }
  }
};