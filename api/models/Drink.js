/**
 * Drink.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    connection: 'someMysqlServer',
    autoPK: false,
    autoCreatedAt: false,
    autoUpdatedAt: false,
    schema: true,
  
    attributes: {
      id: {
        type: 'integer',
        autoIncrement: true,
        primaryKey: true,
        required: true
      },
      portion: {
        type:'float',
        required: true,
      },
      feedtank: {
        type:'string',
        required: true,
        size: 255
      },
      id_recipe: {
        type: 'integer',
        model: 'recipe'
      },
      degree: {
        type:'float',
        required: true,
      },
      name: {
        type:'string',
        required: true,
        size: 255
      }    
    }
};

