/**
 * Staff.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'someMysqlServer',
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  schema: true,

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      required: true
    },
    firstName: {
      type:'string',
      required: true,
      size: 255,
    },
    lastName: {
      type:'string',
      required: true,
      size: 255,
    },
    middleName: {
      type:'string',
      required: true,
      size: 255,
    },
    adress: {
      type:'string',
      required: true,
      size: 255,
    },
    birthDate: {
      type: 'date',
      required: true
    },
    salary: {
      type: 'float',
      required: true
    },
    id_position: {
      type:'integer',
      model:'pos'
    }
  }
};

