/**
 * Recipes.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'someMysqlServer',
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  schema: true,

    attributes: {
      id_recipe: {
        type: 'integer',
        model: 'recipe'
      },
      quantity: {
        type:'integer',
        required: true,
      },
      price: {
        type:'float',
        required: true,
      },
      id_goods: {
        type:'integer',
        model:'goods'
      },
      id_drink: {
        type:'integer',
        model:'drink'
      }
    }
};

