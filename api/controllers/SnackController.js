/**
 * SnackController
 *
 * @description :: Server-side logic for managing snacks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show: function (req, res) {
        Snack.query('SELECT * FROM snack;', [], function (err, snacks) {
            if (err) { return res.serverError(err) };
            res.view('snack/show', { snacks: snacks });
        })
    },

    add: function(req, res) {
        Snack.query('SELECT s.id, s.name FROM snack s;',[],function(err, snack){
            if (err) { return res.serverError(err) };
            Snack.query('SELECT g.id, g.name from goods g INNER JOIN goodsinstock gis ON gis.id = g.id;',[],function(err2, goods){
                if (err2) { return res.serverError(err2) };

                res.view('snack/add', {snacks:snack, goods:goods});
            });
        })
    },
    new: function(req, res) {
        var uv = req.body;
        Snack.query('SELECT IFNULL(MAX(id),0)+1 as s FROM recipe;',[],function(err, id){
            if (err) { return res.serverError(err) };
            id = id[0].s;

            Snack.query('INSERT INTO recipe (id) VALUES(?);',[id], function(err2, result){
                if (err2) { return res.serverError(err2) };

                Snack.query('INSERT INTO recipes (quantity, price, id_goods, id_recipe) VALUES(?,?,?,?);',
                [uv.count, Math.random()*100+Math.random()*50, uv.id_goods, id],function(err3, result){
                    if (err3) { return res.serverError(err3) };
                    Snack.query('INSERT INTO snack (portion, id_recipe, name) VALUES(?,?,?)',
                    [uv.portion, id, uv.name], function(err4, result){
                        if (err4) { return res.serverError(err4) };

                        res.redirect('/snack');
                    });
                });
            });
        });
    },

    delete: function(req, res) {
        var id = req.params.id;

        Snack.query('DELETE FROM snack WHERE id=?',[id],function(err, result){
            if (err) { return res.serverError(err) };

            res.redirect('/snack');
        });
    }
};

