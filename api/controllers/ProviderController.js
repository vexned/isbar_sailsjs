/**
 * ProviderController
 *
 * @description :: Server-side logic for managing Providers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show:function(req, res){
        Provider.query('SELECT p.id, p.name, p.postAdress, p.phone, p.fax, p.email, p.id_requisites FROM provider AS p;',[],function(err, providers){
            if(err) { return res.serverError(err) };

            //sails.log(rawRes);

            //return res(rawRes);
            res.view('provider/show', {providers: providers});
        })
    },
    edit:function(req, res){
        var id = req.params.id;
        Provider.query('SELECT p.id, p.name, p.postAdress, p.phone, p.fax, p.email, p.id_requisites FROM provider AS p WHERE p.id = ?',[id], function(err, provider){
            if(err) { return res.serverError(err) };
            res.view('provider/edit',{provider:provider[0]});
        })
    },
    update:function(req, res){
        var id = req.params.id;
        console.log(id);
        var uv = req.body;
        console.log(uv);
        Provider.query('UPDATE provider SET name=?, postAdress=?, phone=?, fax=?, email=? WHERE provider.id = ?;',
        [uv.name, uv.postAdress, uv.phone, uv.fax, uv.email, id],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.redirect('/provider')
        });
    },
    delete:function(req, res) {
        var id = req.params.id;
        Provider.query('DELETE FROM provider WHERE id = ?',[id],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.redirect('/provider');
        });
    },
    add:function(req, res){
        res.view('provider/add');
    },
    new:function(req, res) {
        var uv = req.body;
        console.log(uv);
        Provider.query('SELECT IFNULL(MAX(id),0)+1 AS id FROM requisites;',[],function(err, ids){
            if(err) {return res.serverError(err);}
            let id = ids[0].id;
            console.log(id);
            Provider.query('INSERT INTO requisites (id, bankName, city, INN, payAccount) VALUES(?, ?, ?, ?, ?);', 
            [id, uv.bankName, uv.city, uv.INN, uv.payAccount], function(err, insres) {
                if(err) {return res.serverError(err);}
                console.log(insres);
                Provider.query('INSERT INTO provider (name, postAdress, phone, fax, email, id_requisites) VALUES(?, ?, ?, ?, ?, ?);',
                [uv.name, uv.postAdress, uv.phone, uv.fax, uv.email, id], function(err, result){
                    console.log(result);
                    res.redirect('/provider');
                });
            });
        });
    },
};

