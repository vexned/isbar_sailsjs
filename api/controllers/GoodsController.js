/**
 * GoodsController
 *
 * @description :: Server-side logic for managing Goods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    show: function (req, res) {
        Goods.query('SELECT g.id, g.name as name, g.bulk, u.name as uname FROM GOODS g INNER JOIN unit u ON u.id = g.id_unit ORDER BY g.id;', [], function (err, goods) {
            if (err) { return res.serverError(err) };

            //sails.log(rawRes);

            //return res(rawRes);
            res.view('goods/show', { goods: goods });
        })
    },
    edit: function (req, res) {
        var id = req.params.id;
        Goods.query('SELECT g.id, g.name, g.bulk, u.name as uname FROM goods AS g INNER JOIN unit u ON u.id = g.id_unit WHERE g.id = ?', [id], function (err, good) {
            if (err) { return res.serverError(err) };

            Unit.query('SELECT * FROM unit', [], function (err, result) {
                if (err) { return res.serverError(err) };

                res.view('goods/edit', { good: good[0], result: result });
                //return res.json(rawRes[0]);
            })
        })
    },
    update: function (req, res) {
        var id = req.params.id;
        console.log(id);
        var uv = req.body;
        console.log(uv);
        Goods.query('UPDATE goods SET name=?, bulk=?, id_unit=? WHERE goods.id=?',
            [uv.name, uv.bulk, uv.id_unit, id], function (err, insertRes) {
                if (err) { return res.serverError(err); }
                console.log(insertRes);
                res.redirect('/goods');
            });
    },
    delete:function(req, res) {
        var id = req.params.id;
        Goods.query('DELETE FROM goods WHERE id = ?',[id],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.redirect('/goods');
        });
    },
    add:function(req, res){
        Staff.query('SELECT * FROM unit',[],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.view('goods/add', {result:result});
        });
    },
    new:function(req, res) {
        var uv = req.body;
        console.log(uv);
        Staff.query('INSERT INTO goods (name, bulk, id_unit) VALUES(?, ?, ?);',
        [uv.name, uv.bulk, uv.id_unit], function(err, insRes){
            if(err) {return res.serverError(err);}
            console.log(insRes);
            res.redirect('/goods');
        });
    },
};

