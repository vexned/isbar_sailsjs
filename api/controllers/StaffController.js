/**
 * StaffController
 *
 * @description :: Server-side logic for managing staff
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	get:function(req, res){
        Staff.find().exec(function(err, staffs) {
            if(err) {
                return res.json(err);
            }
            return res.json(staffs);
        })
    },
    getSql:function(req, res){
        Staff.query('SELECT * FROM staff;',[],function(err, rawRes){
            if(err) { return res.serverError(err) };

            //sails.log(rawRes);

            return res.json(rawRes);
        })
    },
    show:function(req, res){
        Staff.query('SELECT s.id, s.firstName, s.lastName, s.middleName, s.birthDate, s.salary, s.adress, p.name FROM staff AS s INNER JOIN pos AS p ON p.id = s.id_position;',[],function(err, rawRes){
            if(err) { return res.serverError(err) };

            //sails.log(rawRes);

            //return res(rawRes);
            res.view('staff/show', {rawRes: rawRes});
        })
    },
    edit:function(req, res){
        var id = req.params.id;
        Staff.query('SELECT s.id, s.firstName, s.lastName, s.middleName, s.birthDate, s.salary, s.adress, p.name FROM staff AS s INNER JOIN pos AS p ON p.id = s.id_position WHERE s.id = ?;',[id], function(err, rawRes){
            if(err) { return res.serverError(err) };
            Pos.query('SELECT * FROM pos',[], function(err, result){
                if(err) { return res.serverError(err) };
                res.view('staff/edit', {rawRes: rawRes[0], result:result});
                //return res.json(rawRes[0]);
            })
        })
    },
    update:function(req, res){
        var id = req.params.id;
        console.log(id);
        var uv = req.body;
        console.log(uv);
        Staff.query('SELECT s.id_position, s.salary FROM staff AS s WHERE s.id=?',[id],function(err, result){
            if(err) {return res.serverError(err);}
            if(uv.isChecked){
                Staff.query('INSERT INTO staffmoveinfo (movenumber, movedate, id_position, reason, id_staff) SELECT IFNULL(MAX(movenumber),0)+1, CURDATE(), ?, ?, ? FROM staffmoveinfo;',
                            [uv.id_position, uv.reason, id], function(err, insertRes){
                            if(err) {return res.serverError(err);}
                            console.log(insertRes);
                });
            }
            console.log(result);
        });

        Staff.query('UPDATE staff SET firstName=?, lastName=?, middleName=?, adress=?, birthDate=?, salary=?, id_position=? WHERE staff.id =?',
                        [uv.firstName, uv.lastName, uv.middleName, uv.address, uv.birthDate, uv.salary, uv.id_position, id], function(err, updateRes){
                            if(err) {return res.serverError(err);}
                            console.log(updateRes);
                            return res.redirect('/staff');
                        }); 
    },
    add:function(req, res){
        Staff.query('SELECT * FROM pos',[],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.view('staff/add', {result:result});
        });
    },
    new:function(req, res) {
        var uv = req.body;
        console.log(uv);
        Staff.query('INSERT INTO staff (firstName, lastName, middleName, adress, birthDate, salary, id_position) VALUES(?, ?, ?, ?, ?, ?, ?);',
        [uv.firstName, uv.lastName, uv.middleName, uv.address, uv.birthDate, uv.salary, uv.id_position], function(err, insRes){
            if(err) {return res.serverError(err);}
            console.log(insRes);
            res.redirect('/staff');
        });
    },
    delete:function(req, res) {
        var id = req.params.id;
        Staff.query('DELETE FROM STAFF WHERE id = ?',[id],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.redirect('/staff');
        });
    }
};

