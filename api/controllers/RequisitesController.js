/**
 * RequisitesController
 *
 * @description :: Server-side logic for managing Requisites
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show:function(req, res){
        var id = req.params.id;
        Provider.query('SELECT p.name, r.bankName, r.city, r.INN, r.payAccount FROM requisites r INNER JOIN provider p ON p.id_requisites = r.id WHERE r.id=?',[id],function(err, provider){
            if(err) { return res.serverError(err) };

            //sails.log(rawRes);

            //return res(rawRes);
            res.view('requisites/show', {provider: provider[0], id:id});
        })
    },
    edit:function(req, res){
        var id = req.params.id;
        Provider.query('SELECT r.id, p.name, r.bankName, r.city, r.INN, r.payAccount FROM requisites r INNER JOIN provider p ON p.id_requisites = r.id WHERE r.id=?',[id], function(err, provider){
            if(err) { return res.serverError(err) };
            res.view('requisites/edit',{provider:provider[0]});
        })
    },
    update:function(req, res){
        var id = req.params.id;
        console.log(id);
        var uv = req.body;
        console.log(uv);
        Provider.query('UPDATE requisites SET bankName=?, city=?, INN=?, payAccount=? WHERE requisites.id = ?;',
        [uv.bankName, uv.city, uv.INN, uv.payAccount, id],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.redirect('/requisites/'+id);
        });
    },
};

