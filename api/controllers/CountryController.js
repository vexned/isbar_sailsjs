/**
 * CountryController
 *
 * @description :: Server-side logic for managing countries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	get:function(req, res){
        Country.find().exec(function(err, countries) {
            if(err) {
                return res.json(err);
            }
            return res.json(countries);
        })
    },
    getSql:function(req, res){
        Country.query('SELECT * FROM country;',[],function(err, rawRes){
            if(err) { return res.serverError(err) };

            //sails.log(rawRes);

            return res.json(rawRes);
        })
    }

};

