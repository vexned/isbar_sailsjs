/**
 * PricelistController
 *
 * @description :: Server-side logic for managing pricelists
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show:function(req, res) {
        Pricelist.query('SELECT d.id, d.NAME, d.PORTION, sum(rs.price) as price,  u.name AS uname FROM DRINK d INNER JOIN RECIPES rs ON rs.id_recipe = d.id_recipe INNER JOIN GOODS gs ON gs.id = rs.id_goods INNER JOIN UNIT u ON u.id = gs.id_unit GROUP BY d.NAME, d.PORTION, uname UNION SELECT s.id, s.NAME, s.PORTION, sum(rs.price),  u.name AS uname FROM SNACK s INNER JOIN RECIPES rs ON rs.id_recipe=s.id_recipe INNER JOIN GOODS gs ON gs.id = rs.id_goods INNER JOIN UNIT u ON u.id = gs.id_unit GROUP BY s.NAME, s.PORTION, u.name;',
        [], function(err, pricelist) {
            if(err) { return res.serverError(err) };

            res.view('pricelist/show',{pricelist:pricelist});
        });
    },
};

