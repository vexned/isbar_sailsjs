/**
 * RecipeController
 *
 * @description :: Server-side logic for managing recipes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show:function(req, res){
        var id = req.params.id;
        Recipe.query('SELECT d.name, g.name as gname, rs.price, rs.quantity, u.name as uname, rs.id_drink, (SELECT distinct d.name FROM drink d WHERE d.id=rs.id_drink) dr FROM recipes rs INNER JOIN goods g ON g.id = rs.id_goods INNER JOIN unit u ON u.id = g.id_unit INNER JOIN drink d ON d.id_recipe=rs.id_recipe WHERE rs.id_recipe=?;',[id],function(err, recipe){
            if(err) { return res.serverError(err) };

            res.view('recipe/show', {recipe: recipe, id:id});
        })
    },
    showsnack:function(req, res){
        var id = req.params.id;
        Recipe.query('SELECT s.name, g.name as gname, rs.price, rs.quantity, u.name as uname FROM recipes rs INNER JOIN goods g ON g.id = rs.id_goods INNER JOIN unit u ON u.id = g.id_unit INNER JOIN snack s ON s.id_recipe=rs.id_recipe WHERE rs.id_recipe=?;',[id],function(err, recipe){
            if(err) { return res.serverError(err) };

            res.view('snack/recipe', {recipe: recipe, id:id});
        })
    },
};

