/**
 * PosController
 *
 * @description :: Server-side logic for managing Pos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	get:function(req, res){
        Pos.find().exec(function(err, positions) {
            if(err) {
                return res.json(err);
            }
            return res.json(positions);
        })
    },
    getSql:function(req, res){
        Pos.query('SELECT * FROM pos;',[],function(err, rawRes){
            if(err) { return res.serverError(err) };

            //sails.log(rawRes);

            return res.json(rawRes);
        })
    },
    show:function(req, res) {
        Pos.query('SELECT * FROM pos;',[], function(err, result) {
            if(err) { return res.serverError(err) };

            res.view('position/show',{result:result});
        });
    },
    edit:function(req, res){
        var id = req.params.id;
        Pos.query('SELECT * FROM pos WHERE id=?;',[id], function(err, result){
            if(err) { return res.serverError(err) };
            console.log(result);
            res.view('position/edit', {result:result[0]});
        })
    },
    add:function(req, res){
            res.view('position/add');
    },
    new:function(req, res) {
        var uv = req.body;
        console.log(uv);
        Staff.query('INSERT INTO pos (name) VALUES(?);',
        [uv.name], function(err, insRes){
            if(err) {return res.serverError(err);}
            console.log(insRes);
            res.redirect('/position');
        });
    },
    delete:function(req, res){
        var id = req.params.id;
        Pos.query('DELETE FROM pos WHERE id=?;',[id], function(err, result){
            if(err) { return res.serverError(err) };
            res.redirect('/position')
        })
    },
    update:function(req, res){
        var id = req.params.id;
        console.log(id);
        var uv = req.body;
        console.log(uv);
        Pos.query('UPDATE pos SET name=? WHERE pos.id =?',
        [uv.name, id], function(err, updateRes){
            if(err) {return res.serverError(err);}
            console.log(updateRes);
            return res.redirect('/position');
        }); 
    }
};

