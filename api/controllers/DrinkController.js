/**
 * DrinkController
 *
 * @description :: Server-side logic for managing drinks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show: function (req, res) {
        Drink.query('SELECT * FROM drink;', [], function (err, drinks) {
            if (err) { return res.serverError(err) };
            res.view('drink/show', { drinks: drinks });
        })
    },

    add: function(req, res) {
        Drink.query('SELECT d.id, d.name FROM drink d;',[],function(err, drink){
            if (err) { return res.serverError(err) };
            Drink.query('SELECT g.id, g.name from goods g INNER JOIN goodsinstock gis ON gis.id = g.id;',[],function(err2, goods){
                if (err2) { return res.serverError(err2) };

                res.view('drink/add', {drinks:drink, goods:goods});
            });
        })
    },
    new: function(req, res) {
        var uv = req.body;
        Drink.query('SELECT IFNULL(MAX(id),0)+1 as s FROM recipe;',[],function(err, id){
            if (err) { return res.serverError(err) };
            id = id[0].s;

            Drink.query('INSERT INTO recipe (id) VALUES(?);',[id], function(err2, result){
                if (err2) { return res.serverError(err2) };

                Drink.query('INSERT INTO recipes (quantity, price, id_goods, id_recipe, id_drink) VALUES(?,?,?,?,?);',
                [uv.count, Math.random()*100+Math.random()*50, uv.id_goods, id, uv.id_drink],function(err3, result){
                    if (err3) { return res.serverError(err3) };
                    Drink.query('INSERT INTO drink (portion, feedtank, id_recipe, degree, name) VALUES(?,?,?,?,?)',
                    [uv.portion, uv.feedtank, id, uv.degree, uv.name], function(err4, result){
                        if (err4) { return res.serverError(err4) };

                        res.redirect('/drink');
                    });
                });
            });
        });
    },
    delete: function(req, res) {
        var id = req.params.id;

        Drink.query('DELETE FROM drink WHERE id=?',[id],function(err, result){
            if (err) { return res.serverError(err) };

            res.redirect('/drink');
        });
    }
};

