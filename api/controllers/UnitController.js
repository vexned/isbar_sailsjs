/**
 * UnitController
 *
 * @description :: Server-side logic for managing Units
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show:function(req, res){
        Goods.query('SELECT * FROM unit;',[],function(err, units){
            if(err) { return res.serverError(err) };

            //sails.log(rawRes);

            //return res(rawRes);
            res.view('unit/show', {units: units});
        })
    },
    edit:function(req, res){
        var id = req.params.id;
        Provider.query('SELECT u.id, u.name FROM unit AS u WHERE u.id = ?',[id], function(err, unit){
            if(err) { return res.serverError(err) };
            res.view('unit/edit',{unit:unit[0]});
        })
    },
    update:function(req, res){
        var id = req.params.id;
        console.log(id);
        var uv = req.body;
        console.log(uv);
        Provider.query('UPDATE unit SET name=? WHERE unit.id = ?;',
        [uv.name, id],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.redirect('/unit')
        });
    },
    delete:function(req, res) {
        var id = req.params.id;
        Staff.query('DELETE FROM unit WHERE id = ?',[id],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.redirect('/unit');
        });
    },
    add:function(req, res){
        res.view('unit/add');
    },
    new:function(req, res) {
        var uv = req.body;
        console.log(uv);
        Staff.query('INSERT INTO unit (name) VALUES(?);',
        [uv.name], function(err, insRes){
            if(err) {return res.serverError(err);}
            console.log(insRes);
            res.redirect('/unit');
        });
    },
};

