/**
 * AssortmentController
 *
 * @description :: Server-side logic for managing assortments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show:function(req, res){
        var id = req.params.id;
        Provider.query('SELECT p.id, p.name FROM provider p WHERE p.id = ?',[id],function(err, provider){
            if(err) { return res.serverError(err) };
            Assortment.query('SELECT g.id, g.name, g.bulk, u.name AS uname, a.price, a.cond, a.payment FROM assortment a INNER JOIN goods g ON a.id_goods=g.id INNER JOIN unit u ON g.id_unit=u.id WHERE a.id_provider=?;',
            [id],function(err2, assortment) {
                if(err) { return res.serverError(err) };

                res.view('assortment/show', {provider:provider[0], assortment:assortment});
            });
        });
    },
    add:function(req, res){
        var provider= {};
        provider.id = req.params.id;
        Assortment.query('SELECT g.id, g.name FROM goods g',[],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.view('assortment/add', {result:result, provider:provider});
        });
    },
    new:function(req, res){
        var provider= {};
        provider.id = req.params.id;
        uv = req.body;
        Assortment.query('INSERT INTO assortment (id_provider, id_goods, price, cond, payment) VALUES(?,?,?,?,?);',
        [provider.id, uv.id_goods, uv.price, uv.cond, uv.payment], function(err, result){
            if(err) {return res.serverError(err);}

            res.redirect('provider/assortment/'+provider.id);
        });
    },
    delete:function(req, res){
        var provider= {};
        provider.id = req.params.id;
        gid = req.params.gid;
        uv = req.body;
        Assortment.query('DELETE FROM assortment WHERE id_goods=? AND id_provider=?;',[gid, provider.id],
        function(err, result){
            if(err) {return res.serverError(err);}

            res.redirect('provider/assortment/'+provider.id);
        });
        
    },
    get:function(req, res){
        var id = req.params.id;
            Assortment.query('SELECT g.id, g.name, g.bulk, u.name AS uname, a.price, a.cond, a.payment FROM assortment a INNER JOIN goods g ON a.id_goods=g.id INNER JOIN unit u ON g.id_unit=u.id LEFT JOIN goodsinstock gis ON g.id=gis.id WHERE a.id_provider=? AND gis.id IS NULL;',
            [id],function(err, assortment) {
                if(err) { return res.serverError(err) };
                console.log(assortment);
                res.send(assortment);
            });
    },
};

