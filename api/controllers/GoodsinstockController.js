/**
 * GoodsinstockController
 *
 * @description :: Server-side logic for managing goodsinstocks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	show: function (req, res) {
        Goodsinstock.query('SELECT g.id, g.name as name, g.bulk, u.name as uname, gis.quantity, gis.awprice FROM GOODS g INNER JOIN unit u ON u.id = g.id_unit INNER JOIN GOODSINSTOCK gis ON gis.id = g.id ORDER BY g.id;', [], function (err, goods) {
            if (err) { return res.serverError(err) };

            //sails.log(rawRes);

            //return res(rawRes);
            res.view('goodsinstock/show', { goods: goods });
        })
    },
    showdoc: function (req, res) {
        Goodsinstock.query('SELECT g.id, g.name as name, g.bulk, u.name as uname, gis.quantity, gis.awprice FROM GOODS g INNER JOIN unit u ON u.id = g.id_unit INNER JOIN GOODSINSTOCK gis ON gis.id = g.id ORDER BY g.id;', [], function (err, goods) {
            if (err) { return res.serverError(err) };

            //sails.log(rawRes);

            //return res(rawRes);
            res.view('goodsinstock/showdoc', { goods: goods });
        })
    },
    add:function(req, res){
        Goodsinstock.query('SELECT id, name FROM provider;',[],function(err, result){
            if(err) {return res.serverError(err);}
            console.log(result);
            res.view('goodsinstock/add', {result:result});
        });
    },
    new:function(req, res){
        var uv = req.body;
        //Goodsinstock.query('INSERT INTO goodsinstock (awprice, quantity, id) VALUES ((SELECT distinct price as p FROM assortment WHERE id_goods=? AND id_provider=?) + (SELECT distinct price as p FROM assortment WHERE id_goods=? AND id_provider=?)/100*30, ?, ?);',
        Goodsinstock.query('UPDATE goodsinstock SET awprice = ((quantity * awprice) + ((SELECT distinct price FROM assortment WHERE id_goods=? AND id_provider=?)+((SELECT distinct price FROM assortment WHERE id_goods=? AND id_provider=?)/100)*30)*?)/(quantity+?), quantity = quantity + ? WHERE id=?;',
        [uv.id_good, uv.id_provider, uv.id_good, uv.id_provider, uv.quantity, uv.quantity, uv.quantity, uv.id_good], function(err, result){
            if(err) {return res.serverError(err);}

            res.redirect('/gis');
        });
    },
    delete:function(req, res){
        var id = req.params.id;
        Goodsinstock.query('DELETE FROM goodsinstock WHERE id=?',[id],function(err, result){
            if(err) {return res.serverError(err);}

            res.send({'result':'1'});
        });
    },
    count:function(req, res){
        var id = req.params.id;
        console.log('id');
        Goodsinstock.query('SELECT quantity FROM goodsinstock WHERE id=?',[id],function(err, result){
            if(err) {return res.serverError(err);}
            
            res.send({result:result[0]});
        });
    },
    edit:function(req, res){
        var id = req.params.id;
        console.log('id');
        Goodsinstock.query('SELECT a.id_provider, p.name FROM assortment a INNER JOIN provider p ON a.id_provider=p.id WHERE a.id_goods=?',[id],function(err, providers){
            if(err) {return res.serverError(err);}

            res.view('goodsinstock/edit', {providers:providers, id:id});
        })
    },
    getone:function(req, res){
        var pid = req.params.pid;
        var gid = req.params.gid;
        //console.log(id);
        Goodsinstock.query('SELECT g.name, a.price, a.cond, a.payment FROM assortment a INNER JOIN goods g ON g.id=a.id_goods WHERE a.id_goods=? AND a.id_provider=?',[gid,pid],function(err, result){
            if(err) {return res.serverError(err);}

            res.send({good:result[0]});
        })
    }
};

